#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright: 2015, Balasankar C <balasankarc@autistici.org>
# License: GPL-3.0+

from gi import require_version
require_version('Gtk', '3.0')

from gi.repository import Gtk, Pango
import operator
import re
from tqdm import tqdm

class SpellChecker(object):

    def __init__(self):
        dictionaryfile = open('rootwords.txt')  # rootwords.txt')
        self.dictionary = dictionaryfile.readlines()
        dictionaryfile.close()
        self.dictionary = [self.normalize(x.strip()) for x in self.dictionary]
        self.oldindex = {}
        self.chars = []
        self.generate_index()

    def normalize(self, word):
        '''
        Normalize word to single encoding.
        '''
        replace = {'ൽ': 'ല്‍', 'ൻ': 'ന്‍', 'ൾ': 'ള്‍',
                   'ൿ': 'ക്‍', 'ർ': 'ര്‍', 'ൺ': 'ണ്‍'}
        for character in replace:
            word = word.replace(character, replace[character])
        return word

    def generate_index(self):
        '''
        Generate data corpus's index.
        '''
        count = 0
        while count < len(self.dictionary):
            word = self.dictionary[count].decode('utf-8')
            firstchar = word[0]
            self.oldindex[firstchar] = count
            count = count + 1
        self.index = sorted(self.oldindex.items(), key=operator.itemgetter(1))
        for i in self.index:
            self.chars.append(i[0])

    def check(self, word):
        if word in self.dictionary:
            return True
        else:
            return False

    def tokenize(self, a):
        '''
        Tokenize each word to different Aksharams.
        '''
        a = a.decode('utf-8')
        k = u''
        c = u'്'
        g = [u'ാ', u'ി', u'ീ', u'ു', u'ൂ', u'െ', u'േ',
             u'ൈ', u'ൊ', u'ോ', u'ൗ', u'ൌ', u'ൃ', u'ം', u'ഃ']

        k = u''
        gram = []
        for i in a:
            if i == c:
                k = k + i
            else:
                if k:
                    if k[-1] == c or i in g:
                        k = k + i
                    else:
                        if gram:
                            if gram[-1] != k:
                                gram.append(k)
                        k = i
                else:
                    k = i
                if a.index(i) < len(a) - 1:
                    if a[a.index(i) + 1] != c and a[a.index(i) + 1] not in g:
                        gram.append(k)
                else:
                    gram.append(k)
        result = u''
        for i in gram:
            result = result + i
        # print " ,".join(gram)
        return gram

    def similarity_check(self, word1, word2):
        '''
        Check Jaccard's similarity between two words.
        '''
        a = self.tokenize(word1)
        b = self.tokenize(word2)
        # print "Tokens of %s" % word1
        # print " ,".join(a)
        # print "Tokens of %s" % word2
        # print " ,".join(b)
        count = 0
        n = 2
        a_gram = []
        b_gram = []
        while count < len(a) - 1:
            a_gram.append(a[count:count + n])
            count = count + 1
        count = 0
        while count < len(b) - 1:
            b_gram.append(b[count:count + n])
            count = count + 1
        common_grams = [x for x in a_gram if x in b_gram]
        all_grams = []
        c_grams = a_gram + b_gram
        all_grams = c_grams
        similarity = float(len(common_grams)) / float(len(all_grams))
        return similarity

    def generate_suggestions(self, word):
        '''
        Return similar correct words of input incorrect word.
        '''
        suggestions = {}
        currentchar = word.decode('utf-8')[0]
        prevchar = self.chars[self.chars.index(currentchar) - 1]
        nextchar = self.chars[self.chars.index(currentchar) + 1]
        if self.chars.index(currentchar) == 0:
            prevposition = 0
        else:
            for i in self.index:
                if i[0] == prevchar:
                    prevposition = i[1]
                    break
        if self.chars.index(nextchar) == len(self.chars) - 1:
            nextposition = len(self.dictionary)
        else:
            for i in self.index:
                if i[0] == nextchar:
                    nextposition = i[1]
                    break
        for rootword in self.dictionary[prevposition:nextposition]:
            similarity = self.similarity_check(word, rootword)
            suggestions[rootword] = similarity

        sorted_suggestions = sorted(suggestions.items(),
                                    key=operator.itemgetter(1))
        newlist = sorted_suggestions[::-1]
        final_list = []
        for i in newlist[:500]:
            len_word = len(self.tokenize(word))
            len_suggestion = len(self.tokenize(i[0]))
            if len_suggestion >= len_word - 2 and len_suggestion <= len_word + 2:
                if i[1] > 0:
                    final_list.append(i)
        return final_list[:10]


class TextViewWindow(Gtk.Window):

    def __init__(self):
        '''
        Initialize the window and dictionary.
        '''
        Gtk.Window.__init__(self, title="Malayalam Spellchecker")
        dictionaryfile = open('rootwords.txt')
        self.dictionary = dictionaryfile.readlines()
        dictionaryfile.close()
        self.dictionary = [x.strip() for x in self.dictionary]
        self.set_default_size(500, 500)
        self.grid = Gtk.Grid()
        self.add(self.grid)
        self.menu = Gtk.Menu()
        self.incorrect_word = ''
        self.spell_checker = SpellChecker()

        self.create_textview()
        self.create_buttons()
        self.corrections = {'asdf': ['qewr']}

    def create_textview(self):
        '''
        Create the TextView in GUI.
        '''
        self.scrolledwindow = Gtk.ScrolledWindow()
        self.scrolledwindow.set_hexpand(True)
        self.scrolledwindow.set_vexpand(True)
        self.grid.attach(self.scrolledwindow, 0, 1, 4, 1)
        self.textview = Gtk.TextView()
        self.textbuffer = self.textview.get_buffer()
        self.scrolledwindow.add(self.textview)
        self.textview.connect("button-press-event", self.button_clicked)
        self.textview.connect("populate-popup", self.menu_creation)
        self.tag_error = self.textbuffer.create_tag(
            "error", underline=Pango.Underline.SINGLE, foreground="red")
        self.tag_found = self.textbuffer.create_tag(
            "found", background="yellow")

    def menu_creation(self, widget, popup):
        '''
        Create the popup menu for suggestions.
        '''
        sub_menu = Gtk.Menu()
        incorrect_word = self.incorrect_word[
            0].get_text(self.incorrect_word[1]).strip()
        if incorrect_word in self.corrections:
            for suggestion in self.corrections[incorrect_word]:
                name = suggestion
                menu_item = Gtk.MenuItem(name=name, label=name)
                menu_item.connect("activate", self.menuitem_response, name)
                sub_menu.append(menu_item)
            replace_menu = Gtk.MenuItem("Replace")
            replace_menu.set_submenu(sub_menu)
            popup.append(replace_menu)
        popup.show_all()

    def menuitem_response(self, widget, name):
        '''
        Replace the selected word by selected suggestion.
        '''
        self.textbuffer.delete(self.incorrect_word[0], self.incorrect_word[1])
        self.textbuffer.insert(self.incorrect_word[0], name)
        self.textbuffer.place_cursor(self.incorrect_word[0])
        return

    def button_clicked(self, widget, event):
        '''
        Handle what happens when clicked inside TextView.
        '''
        if event.button == 3:  # If a right click
            start_iter = self.textbuffer.get_start_iter()
            end_iter = self.textbuffer.get_end_iter()
            self.textbuffer.remove_tag(self.tag_found, start_iter, end_iter)
            x, y = self.textview.get_pointer()
            x, y = self.textview.window_to_buffer_coords(
                Gtk.TextWindowType.TEXT, x, y)
            iter = self.textview.get_iter_at_location(x, y).iter
            iter2 = iter.copy()
            iter.backward_word_start()
            iter2.forward_word_end()
            self.textbuffer.apply_tag(self.tag_found, iter, iter2)
            self.incorrect_word = [iter, iter2]

    def create_buttons(self):
        '''
        Create necessary buttins in UX.
        '''
        button_error = Gtk.Button("SpellCheck")
        self.grid.attach(button_error, 0, 2, 4, 1)
        button_error.connect("clicked", self.on_spellcheck_button_clicked,
                             self.tag_error)

    def on_spellcheck_button_clicked(self, widget, tag):
        '''
        Handles the click action of spellcheck button.
        '''
        cursor_mark = self.textbuffer.get_insert()
        start = self.textbuffer.get_iter_at_mark(cursor_mark)
        if start.get_offset() == self.textbuffer.get_char_count():
            start = self.textbuffer.get_start_iter()
        start_iter = self.textbuffer.get_start_iter()
        end_iter = self.textbuffer.get_end_iter()
        text = self.textbuffer.get_text(start_iter, end_iter, True)
        input_words = text.split()
        correct_words, incorrect_words = self.spellcheck(input_words)
        for word in incorrect_words:
            self.search_and_mark(word, start)

    def spellcheck(self, input_words):
        '''
        Perform spellcheck operation.
        '''
        mix_word_regex = re.compile('.*\w+.*')
        number_word_regex = re.compile('.*\d+.*')
        correct_words = []
        incorrect_words = []
        for word in tqdm(input_words):
            print word
            if mix_word_regex.match(word):
                # Detect different languages
                print "Mixed language or number detected"
                continue
            if number_word_regex.match(word):
                # Detect different languages
                print "Mixed language or number detected"
                continue
            word = self.spell_checker.normalize(word)
            status = self.spell_checker.check(word)
            if status:
                correct_words.append(word)
            else:
                incorrect_words.append(word)
                suggestions = self.spell_checker.generate_suggestions(word)
                if suggestions:
                    if word not in self.corrections:
                        self.corrections[word] = []
                for i in suggestions:
                    # print i[0], i[1]
                    self.corrections[word].append(i[0])

        return correct_words, incorrect_words

    def search_and_mark(self, text, start):
        '''
        Mark incorrect words in red color.
        '''
        end = self.textbuffer.get_end_iter()
        match = start.forward_search(text, 0, end)
        if match is not None:
            match_start, match_end = match
            self.textbuffer.apply_tag(self.tag_error, match_start, match_end)
            self.search_and_mark(text, match_end)


if __name__ == "__main__":
    win = TextViewWindow()
    win.connect("delete-event", Gtk.main_quit)
    win.show_all()
    Gtk.main()
